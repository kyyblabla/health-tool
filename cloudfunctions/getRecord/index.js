// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database({
  throwOnNotFound: false
})
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const {
    date
  } = event
  const {
    list
  } = await db.collection('record')
    .aggregate()
    .match({
      '_openid': wxContext.OPENID,
      'date': date,
      deleted: _.neq(1)
    })
    .lookup({
      from: 'homemate',
      localField: 'homemate',
      foreignField: '_id',
      as: 'homemateInfo'
    })
    .sort({
      addTime: -1
    })
    .end()
  return list
}