// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database({
  throwOnNotFound: false
})
const _ = db.command
const $ = db.command.aggregate

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const {
    date
  } = event
  // 获取当月数据
  const ym = date.substring(0, 7)
  const {
    list
  } = await db.collection('record').aggregate().match({
    '_openid': wxContext.OPENID,
    'date': db.RegExp({
      regexp: `${ym}.+`,
      options: 'i',
    }),
    deleted: _.neq(1)
  }).group({
    _id: '$date',
    num: $.sum(1)
  }).end()
  return list
}