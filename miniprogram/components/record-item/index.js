// components/record-item/index.js
import {
  format
} from '../../util/index'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    itemInfo: {}
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onClick() {
      this.triggerEvent('click')
    }
  },
  ready() {
    let info = ""
    let styleClass = ""
    const r = this.data.item
    if (r.tempValue) {
      info = `体温:${r.tempValue}℃ `
      if (r.tempValue >= 37) {
        styleClass = 'error'
      }
      if (r.tempValue < 36) {
        styleClass = 'warning'
      }
    }
    if (r.tripLocation) {
      info = `${info}出行:${r.tripLocation}`
      if (!styleClass) {
        styleClass = 'warning'
      }
    }
    const itemInfo = {
      name: `${r._homemate.name}`,
      value: info,
      styleClass,
      time: format(new Date(r.addTime), 'hh:mm')
    }
    this.setData({
      itemInfo
    })
  }
})