//index.js
import {
  format
} from '../../util/index'
import {
  wxp
}
from '../../util/wxp';
const db = wx.cloud.database()
Page({
  data: {
    calendarView: 'day',
    originRecords: [],
    records: [],
    currentDay: null,
    calendarConfig: {
      theme: 'elegant',
      defaultDay: true,
      showLunar: true,
      highlightToday: true,
      disableLaterDay: true, // 是否禁选当天之后的日期
    }
  },
  onLoad: function () {},
  afterCalendarRender() {
    let currentDay = format(new Date(), 'yyyy-MM-dd')
    this.setData({
      currentDay
    })
    this.updateData()
  },
  async afterTapDay(e) {
    let currentDay = format(new Date(), 'yyyy-MM-dd')
    const {
      year,
      month,
      day
    } = e.detail
    currentDay = `${year}-${month}-${day}`
    currentDay = format(new Date(currentDay), 'yyyy-MM-dd')
    this.setData({
      currentDay
    })
    wx.showLoading({
      title: '努力加载中...',
    })
    await this.getRecord()
    wx.hideLoading()
  },
  async getRecord() {
    this.setData({
      records: []
    })
    const {
      result
    } = await wxp.cloud.callFunction({
      name: 'getRecord',
      data: {
        date: this.data.currentDay
      }
    })
    const records = result.map(r => {
      r._homemate = r.homemateInfo[0]
      return r
    })
    this.setData({
      originRecords: result
    })
    this.setData({
      records
    })
  },
  /**
   * 月份改变事件
   */
  async whenChangeMonth({
    detail
  }) {
    const {
      next
    } = detail
    let currentDay = `${next.year}-${next.month}-01`
    currentDay = format(new Date(currentDay), 'yyyy-MM-dd')
    this.setData({
      currentDay
    })
    // 设置当月数据
    this.updateData()
    this.calendar.jump(next.year, next.month, 1);
    // 跳到当月1号
  },
  async getMonthRecord() {
    const {
      result
    } = await wxp.cloud.callFunction({
      name: 'getMonthRecord',
      data: {
        date: this.data.currentDay
      }
    })
    const days = result.map(r => {
      const strs = r._id.split('-')
      return {
        year: strs[0],
        month: strs[1],
        day: strs[2]
      }
    })
    this.calendar.clearTodoLabels()
    this.calendar.setTodoLabels({
      // 待办点标记设置
      pos: 'bottom', // 待办点标记位置 ['top', 'bottom']
      dotColor: '#509aff', // 待办点标记颜色
      days
    });
  },
  onPullDownRefresh() {
    this.updateData()
    wx.stopPullDownRefresh()
  },
  async updateData() {
    wx.showLoading({
      title: '努力加载中...',
    })
    await this.getRecord()
    await this.getMonthRecord()
    wx.hideLoading()
  },
  getRecordById(id) {
    return this.data.originRecords.filter(e => e._id == id)[0]
  },
  onAddRecord() {
    wx.navigateTo({
      url: '/pages/record/record?date=' + this.data.currentDay
    })
  },
  onViewRecord(e) {
    const id = e.currentTarget.dataset.id
    const date = this.data.currentDay
    wx.navigateTo({
      url: `/pages/record/record?date=${date}&_id=${id}`
    })
  },
  onShareAppMessage(options) {
    return {
      title: '康阿姨-社区健康统计助手',
      path: '/pages/login/login'
    }
  }
})