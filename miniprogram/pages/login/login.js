// miniprogram/pages/login/login.js
import {
  wxp
}
from '../../util/wxp';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    needSeting: false
  },

  onLoad: async function ({
    from
  }) {
    if (!wx.cloud) {
      wx.redirectTo({
        url: '../chooseLib/chooseLib',
      })
      return
    }
    // 获取用户信息
    this.init(from)
  },
  init: async function (from) {
    // 获取用户已经绑定的个人信息,没有绑定，保留在登陆页面
    const {
      result
    } = await wxp.cloud.callFunction({
      name: 'login',
      data: {}
    })
    console.log(result);
    getApp().globalData.openId = result.user._id
    const baseInfo = result.user.baseInfo || {}
    // 个人信息编辑完毕，跳到首页
    if (baseInfo.community) {
      if (from !== 'my') {
        wx.switchTab({
          url: '/pages/index/index'
        })
      }
    } else {
      this.setData({
        needSeting: true
      })
    }
  },
  onSetting() {
    wx.redirectTo({
      url: '/pages/my/setting/setting?from=login',
    })
  }
})