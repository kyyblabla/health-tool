// miniprogram/pages/my/index/index.js
import {
  wxp
} from '../../../util/wxp'
const db = wx.cloud.database()
const _ = db.command

Page({

  /**
   * 页面的初始数据
   */
  data: {
    wxUserInfo: {
      'avatarUrl': 'https://6465-dev-g9vl3-1301207708.tcb.qcloud.la/avatar.png?sign=f342c0bb685f3f34bdb4d85b4413950d&t=1580996435'
    },
    userInfo: {},
    community: {},
    homemateCount: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    this.getUserInfo()
  },
  async getUserInfo() {
    wx.showLoading({
      title: '努力加载中...',
    })
    const openId = getApp().globalData.openId
    const {
      data: user
    } = await db.collection('user').doc(openId).get()

    this.setData({
      userInfo: user.baseInfo,
    })

    if (user.baseInfo.community) {
      const {
        data
      } = await db.collection('community').doc(user.baseInfo.community).get()
      this.setData({
        community: data,
      })
    }
    const {
      total
    } = await db.collection('homemate').where({
      '_openid': openId,
      deleted: _.neq(1)
    }).count()

    this.setData({
      homemateCount: total
    })
    wx.hideLoading()
  },
  onClickUserInfo() {
    wx.navigateTo({
      url: '/pages/my/setting/setting',
    })
  },
  onViewHomemates() {
    wx.navigateTo({
      url: '/pages/homemates/homemates?from=setting',
    })
  },
  async onPullDownRefresh() {
    await this.getUserInfo()
    wx.stopPullDownRefresh()
  },
  onAboutUs() {
    wx.navigateTo({
      url: '/pages/login/login?from=my',
    })
  },
  onFeedback() {
    wx.navigateTo({
      url: '/pages/my/feedback/feedback',
    })
  }
})