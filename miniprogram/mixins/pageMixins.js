module.exports = {
  data: {
    popup: null
  },
  getPrevPage() {
    const pages = getCurrentPages();
    const prevPage = pages[pages.length - 2]; //上一个页面     
    return prevPage
  },
  closePopup: function () {
    this.setData({
      popup: null
    });
  },
  showPopup: function (e) {
    const filed = e.currentTarget.dataset.filed
    this.setData({
      popup: filed
    })
  },
  onFiledChange: function (e) {
    const filed = e.currentTarget.dataset.filed
    const value = e.detail
    this.setData({
      ['form.' + filed]: value
    })
  }
}